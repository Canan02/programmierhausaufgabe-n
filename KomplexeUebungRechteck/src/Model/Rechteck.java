package Model;

import java.util.Random;

public class Rechteck {
	private int x;
	private int y;
	private int breite;
	private int hoehe;
	private Punkt p;
	
	
	
	public Rechteck() {
		
		
	}



	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.x = x;
		this.y = y;
		this.breite = breite;
		this.hoehe = hoehe;
	}



	public int getX() {
		return x;
	}



	public void setX(int x) {
		this.x = x;
	}



	public int getY() {
		return y;
	}



	public void setY(int y) {
		this.y = y;
	}



	public int getBreite() {
		return breite;
	}



	public void setBreite(int breite) {
		this.breite = breite;
	}



	public int getHoehe() {
		return hoehe;
	}
	public boolean enthaelt1(int x, int y) { 		
		if (x>=this.p.getX() && y>=this.p.getY() && 
				x<=this.breite && y<=this.hoehe) {return true;} 		
		else {return false;} 	} 	 	
	public boolean enthaelt1(Punkt p) { 		
		return enthaelt1(p.getX(),p.getY()); 	
		}

	public boolean enthaelt(int breite, int getHoehe) { 		
		if (x>=this.p.getX() && y>=this.p.getY() && 
				x<=this.breite && y<=this.hoehe) {return true;} 		
		else {return false;} 	} 	 	
	public boolean enthaelt(Rechteck rechtecke) { 		
		return enthaelt1(rechtecke.getX(),rechtecke.getY()); 	
		}
	
	public static Rechteck generiereZufallsRechteck() { 		
		Random random = new Random (); 		 		
		Rechteck rechteck = new Rechteck(random.nextInt(1200),random.nextInt(1000),random.nextInt(1200),random.nextInt(1000)); 		
		return rechteck; 	
		}

	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", breite=" + breite + ", hoehe=" + hoehe + ", getX()=" + getX()
				+ ", getY()=" + getY() + ", getBreite()=" + getBreite() + ", getHoehe()=" + getHoehe() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}



	public int setHoehe(int hoehe) {
		this.hoehe = hoehe;
		return hoehe;
	}


}